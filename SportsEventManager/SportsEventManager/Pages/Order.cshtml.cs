using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SportsEventManager.Services.Events;

namespace SportsEventManager.Web.Pages
{
    public class OrderModel : PageModel
    {
        private readonly IOrderService _orderService;
        public OrderModel(IOrderService orderService)
        {
            _orderService = orderService;
        }

        void OnPostSubmit()
        {
            _orderService.SubmitOrderRequest(DateTime.Now, "Nevim", "Neznam");
        }

        public void OnGet()
        {

        }
    }
}