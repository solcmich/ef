﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SportsEventManager.Services.Events
{
    public interface IOrderService
    {
        void SubmitOrderRequest(DateTime date, string address, string eventName);

        void ConfirmOrder(int id, string message);


        void CancelOrder();

        void OrderPayed();
    }
}
